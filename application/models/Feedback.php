<?php

	class Feedback{

		public static function Save($name, $message, $email){
			global $db;
			$sql = 'INSERT INTO feedback (id, name, message, email, date_created) VALUES(:id, :name, :message, :email, :date_created);';
			$stm = $db->prepare($sql);

			try{
				$db->beginTransaction();
				$result = $stm->execute(
					array(
						':id' => self::getGuId(),
						':name' => $name,
						':message' => $message,
						':email' => $email,
						':date_created' => date("Y-m-d G:i:s"),
					)
				);
				$db->commit();

				return $result;
			}
			catch(PDOException $ex){
				$db->rollBack();
				Logger::getRootLogger()->error($ex->getMessage());

				return null;
			}
		}

		public function getGuId(){
			if(function_exists('com_create_guid')){
				return com_create_guid();
			}
			else{
				mt_srand((double)microtime() * 10000);//optional for php 4.2.0 and up.
				$charid = strtoupper(md5(uniqid(rand(), true)));
				$hyphen = chr(45);// "-"
				$uuid = chr(123)// "{"
					. substr($charid, 0, 8) . $hyphen . substr($charid, 8, 4) . $hyphen . substr(
						$charid, 12, 4
					) . $hyphen . substr($charid, 16, 4) . $hyphen . substr($charid, 20, 12) . chr(125);// "}"
				$uuid = str_replace('{', '', $uuid);
				$uuid = str_replace('}', '', $uuid);

				return strtolower($uuid);
			}
		}

	}