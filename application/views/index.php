<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="/application/templates/css/Style.css"/>
    <script type="text/javascript" src="/application/templates/js/jquery-1.11.1.min.js"></script>
    <script src="/application/templates/js/feedback.js"></script>
    <title>Feedback</title>
</head>
<body>
<div id="form">
    <form id="form" method="post">
        <span id="error_label"></span><br/>
        <div id="form-row">
            <input type="text" id="name" placeholder="Ф.И.О" maxlength="32"/>
            <span id="error_name"></span><br/>
        </div>
        <div id="form-row">
            <textarea id="message"  placeholder="Сообщение..." maxlength="255"></textarea>
            <span id="error_message"></span><br/>
        </div>
        <div id="form-row">
            <input type="text" id="email" placeholder="E-mail" maxlength="64"/>
            <span id="error_email"></span><br/>
        </div>
        <input type="button" onclick="check()" value="Отправить"/>
        <input type="button" onclick="formClear()" value="Очистить"/>
    </form>
</div>
<div class="popup" id="popupend">
   <span id="popup_end"></span>
    <div id="close_popup"></div>
</div>
<div id="overlay"></div>
</body>
</html>