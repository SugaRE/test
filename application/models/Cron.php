<?php

	class Cron{

		public static function load(){
			global $db;
			$sql = 'SELECT * FROM feedback WHERE sent=0 AND date_created= :pattern ORDER BY date_created ;';
			$stm = $db->prepare($sql);

			try{
				$stm->bindParam(':pattern', date("Y-m-d G:i:s", time()-10*60));
				$stm->execute();

				$i = 0;
				while ($row = $stm->fetch()) {
					$data[$i]['id'] = $row['id'];
					$data[$i]['name'] = $row['name'];
					$data[$i]['message'] = $row['message'];
					$data[$i]['email'] = $row['email'];
					$i++;
				}
				return $data;

			}
			catch(PDOException $ex){
				Logger::getRootLogger()->error($ex->getMessage());

				return null;
			}
		}

		public static function checkDomain($email){

			$blackList = array(
				'mail.ru',
				'rambler.ru',
				'yandex.ru',
			);

			$domain = explode('@', $email);

			if(!in_array($domain[1], $blackList)){
				return true;
			}
			else{
				return false;
			}

		}

		public static function updateSent($id){
			global $db;
			$sql = 'UPDATE feedback SET sent= 1 WHERE id = :id;';
			$stm = $db->prepare($sql);

			try{
				$db->beginTransaction();
				$stm->bindParam(':id', $id);
				$result = $stm->execute();
				$db->commit();

				return $result;
			}
			catch(PDOException $ex){
				$db->rollBack();
				Logger::getRootLogger()->error($ex->getMessage());

				return false;
			}
		}

		public static function createMail($name, $message, $email, bool $pattern){
			$domain = explode('@', $email);
			if($pattern){
				$string = "{$name}, добрый день! Очень рады, что вы выбрали {$domain[1]}. Ваш запрос: \"{$message}\" будет обработан в ближайшее время. Удачи!";

				return $string;
			}
			else{
				$string = "{$name}, добрый день! Мы не можем вам ничем помочь, т.к. Не работаем с клиентами {$domain[1]}";

				return $string;
			}

		}

		public static function sentMail($email, $message, bool $domainValid){

			if($domainValid){
				return mail($email, 'Всё отлично', $message);
			}
			else{
				return mail($email, 'К сожалению, мы не можем вам помочь', $message);
			}
		}

	}