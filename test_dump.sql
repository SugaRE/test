-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Апр 26 2017 г., 14:44
-- Версия сервера: 5.5.53
-- Версия PHP: 7.0.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `test`
--

-- --------------------------------------------------------

--
-- Структура таблицы `feedback`
--

CREATE TABLE `feedback` (
  `id` varchar(36) NOT NULL,
  `name` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `email` varchar(255) NOT NULL,
  `date_created` datetime NOT NULL,
  `sent` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `feedback`
--

INSERT INTO `feedback` (`id`, `name`, `message`, `email`, `date_created`, `sent`) VALUES
('2943dc59-8c9e-00d2-86ba-7ee7f75d5ac1', 'Тестов Аркадий Петрович', 'Не следует, однако забывать, что реализация намеченных плановых заданий влечет за собой процесс внедрения и модернизации системы обучения кадров, соответствует насущным потребностям.', 'info@mail.ru', '2017-04-26 14:32:00', 1),
('2c8bbcdb-fe7e-0bc4-991c-365c152ed47c', 'Тестов Аркадий Петрович', 'Не следует, однако забывать, что реализация намеченных плановых заданий влечет за собой процесс внедрения и модернизации системы обучения кадров, соответствует насущным потребностям.', 'info@yandex.ru', '2017-04-26 14:33:00', 1),
('819c210a-b06c-678c-2369-b873213eb4ae', 'Тестов Аркадий Петрович', 'Не следует, однако забывать, что реализация намеченных плановых заданий влечет за собой процесс внедрения и модернизации системы обучения кадров, соответствует насущным потребностям.', 'info@rambler.ru', '2017-04-26 14:33:00', 1),
('89b5bc45-ba3e-70e4-55fe-d331a99a7523', 'Тестов Аркадий Петрович', 'Не следует, однако забывать, что реализация намеченных плановых заданий влечет за собой процесс внедрения и модернизации системы обучения кадров, соответствует насущным потребностям.', 'info@bk.ru', '2017-04-26 14:32:00', 1),
('b7a747d7-5fd7-46c1-2747-fe8bdeb05df8', 'Тестов Аркадий Петрович', 'Не следует, однако забывать, что реализация намеченных плановых заданий влечет за собой процесс внедрения и модернизации системы обучения кадров, соответствует насущным потребностям.', 'info@gmail.com', '2017-04-26 14:32:00', 1),
('c720e37d-e250-277e-5143-58a6ff6b6100', 'Тестов Аркадий Петрович', 'Не следует, однако забывать, что реализация намеченных плановых заданий влечет за собой процесс внедрения и модернизации системы обучения кадров, соответствует насущным потребностям.', 'info@mail.ua', '2017-04-26 14:33:00', 1),
('e7c3ef24-2ce5-0b71-d1a2-4801a3ee75f4', 'Тестов Аркадий Петрович', 'Не следует, однако забывать, что реализация намеченных плановых заданий влечет за собой процесс внедрения и модернизации системы обучения кадров, соответствует насущным потребностям.', 'info@inbox.ru', '2017-04-26 14:32:00', 1);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `feedback`
--
ALTER TABLE `feedback`
  ADD UNIQUE KEY `id` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
