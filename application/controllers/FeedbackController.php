<?php

	require_once(ROOT . '/application/models/Feedback.php');
	require_once(ROOT . '/application/models/Cron.php');

	class FeedbackController extends Controller{

		public function actionIndex(){
			$this->view->render('index');

		}

		public function actionCurl(){

			$done = Feedback::Save($_POST['name'], $_POST['message'], $_POST['email']);
			if($done){
				echo '{"response":{"body":"Запрос созранен удачно.", "error":"0"}}';
			}
			else{
				echo '{"response":{"body":"Неизвестная ошибка. Запрос не сохранен.", "error":"1"}}';
			}

		}

		public function actionAjax(){
			$json = $_POST;

			$curl = curl_init();
			curl_setopt($curl, CURLOPT_URL, "http://{$_SERVER['SERVER_ADDR']}/feedback/curl");
			curl_setopt($curl, CURLOPT_HEADER, 0);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($curl, CURLOPT_POST, 1);
			curl_setopt($curl, CURLOPT_POSTFIELDS, $json);
			$response = curl_exec($curl);

			echo json_encode($response);

			return true;
		}

		public function actionCron(){

			$arr = Cron::load();
			if($arr){
				foreach($arr as $data){
					$domainValid = Cron::checkDomain($data['email']);
					$mail = Cron::createMail($data['name'], $data['message'], $data['email'], $domainValid);
					$mailSent = Cron::sentMail($data['email'], $mail, $domainValid);
					if($mailSent){
						Cron::updateSent($data['id']);
					}
				}
			}
			else{
				exit('end');
			}

		}
	}