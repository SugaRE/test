<?php
	$config = array(
		'db' => array(
			'host' => 'localhost',
			'dbname' => 'test',
			'user' => 'root',
			'password' => '60a0714f34aff3100367e5e5b5c26d4c',
		),
		'permissions' => array(
			'admin' => 200,
			'user' => 100,
			'newuser' => 50,
			'banned' => -100,
		),
		'version' => '3.5',
	);