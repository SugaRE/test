function check() {
    reset();

    var name = document.getElementById('name').value;
    var message = document.getElementById('message').value;
    var email = document.getElementById('email').value;

    var error = false;

    if ((name.match(/^[А-ЯЁ][а-яё]+ [А-ЯЁ][а-яё]+ [А-ЯЁ][а-яё]+$/) == null) || (name.length < 8)) {
        error = true;
        document.getElementById('error_name').innerHTML = 'Неверный формат имени';
        document.getElementById('name').style.backgroundColor = "#ff4d56";
    }

    if (!(message.length > 10)) {
        error = true;
        document.getElementById('error_message').innerHTML = 'Сообщение слишком короткое.';
        document.getElementById('message').style.backgroundColor = "#ff4d56";
    }

    if (email.match(/^([a-z0-9_-]+\.)*[a-z0-9_-]+@[a-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,6}$/) == null) {
        error = true;
        document.getElementById('error_email').innerHTML = 'Вы указали неверный E-mail';
        document.getElementById('email').style.backgroundColor = "#ff4d56";
    }


    if (!error) {
        $.ajax({
            type: 'POST',
            url: '/feedback/ajax',
            dataType: 'json',
            data: {
                name: name,
                message: message,
                email: email
            },
            success: function(res) {
                var resp = jQuery.parseJSON(res).response;
                document.getElementById('popup_end').innerHTML += resp.body;
                $(' #popupend, #overlay').fadeIn(300);

            }
        });

    }
}

function reset() {
    document.getElementById('error_name').innerHTML = '';
    document.getElementById('error_message').innerHTML = '';
    document.getElementById('error_email').innerHTML = '';
    document.getElementById('name').style.backgroundColor = "#ffffff";
    document.getElementById('message').style.backgroundColor = "#ffffff";
    document.getElementById('email').style.backgroundColor = "#ffffff";
}

function formClear() {
    $('form[id=form]').trigger('reset');
    reset();
}

$(document).ready(function () {
    $('#overlay').css({opacity: 0.5});
    positionCenter($('.popup'));

    $(' #overlay').click(function () {
        $('.popup, #overlay').fadeOut(300);
        document.getElementById('popup_end').innerHTML = '';
    });

    $(' #close_popup').click(function () {
        $('.popup, #overlay').fadeOut(300);
        document.getElementById('popup_end').innerHTML = '';
    });

    function positionCenter(elem) {
        elem.css({
            marginTop: '-' + elem.height() / 2 + 'px',
            marginLeft: '-' + elem.width() / 2 + 'px'
        });
    }
});