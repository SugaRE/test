<?php
	define('ROOT', dirname(__FILE__));
	
		require_once ROOT . '/config/config.php';
		require_once ROOT . '/core/View.php';
		require_once ROOT . '/core/Controller.php';
		require_once ROOT . '/core/DataBase.php';
		require_once ROOT . '/core/include/log4php/Logger.php';
		require_once ROOT . '/application/controllers/FeedbackController.php';
		
		$db = DataBase::getConnection();
	
	$feedbackController = new FeedbackController();
	$feedbackController->actionCron();
	
	